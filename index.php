<!DOCTYPE html>
<html lang="ru">

<head>
    <title>Mine Test</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <!-- jquery -->
    <script src="vendor/jquery/jquery-3.4.1.min.js"></script>

    <!-- bootstrap -->
    <link rel="stylesheet" href="vendor/bs/bootstrap.min.css">
    <script src="vendor/bs/bootstrap.bundle.min.js"></script>

    <!-- datepicker -->
    <script type="text/javascript" src="vendor/datepicker/js/bootstrap-datepicker.min.js" charset="UTF-8"></script>
    <script type="text/javascript" src="vendor/datepicker/js/bootstrap-datepicker.ru.min.js" charset="UTF-8"></script>
    <link rel="stylesheet" href="vendor/datepicker/css/bootstrap-datepicker.min.css">

    <!-- viewbox -->
    <script type="text/javascript" src="vendor/viewbox/jquery.viewbox.min.js" charset="UTF-8"></script>
    <link rel="stylesheet" href="vendor/viewbox/viewbox.css">

    <script src="main.js"></script>

</head>

<body>

<!-- top menu -->
<div id="c_menu" class="container-fluid mt-2" >
    <div class="row">

        <label for="course" class="col-md-1 mt-2">Курс:&nbsp;</label>
        <div class="col-md-2 mt-2 mt-md-0">
            <input id="course" type="text" class="form-control" placeholder="курс">
        </div>

        <div class="col-md-3 mt-2 mt-md-0">
            <button id="btn_set_course" type="button" class="btn btn-primary">Установить курс</button>
        </div>

        <div class="col-md-2 mt-2 mt-md-0">
            <input id="datepicker" type="text" class="form-control" placeholder="Месяц">
        </div>

        <div class="col-md-4 mt-2 mt-md-0">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#c_add_worker">Добавить сотрутника</button>
        </div>

        <label for="premium_amount" class="col-md-3 mt-2 col-form-label">Выдать премию за выбранный месяц:&nbsp;</label>
        <div class="col-md-3 mt-2">
            <input id="premium_amount" type="text" class="form-control" placeholder="Сумма">
        </div>

        <div class="col-md-3 mt-2">
                <select class="form-control" id="premium_profession">
<?php

require_once __DIR__ . '/Workers.php';

$workers = new Workers();
$workers->printProfessions();

?>
                </select>
        </div>

        <div class="col-md-3 mt-2">
            <button id="btn_add_premium" type="button" class="btn btn-primary">Выдать</button>
        </div>

        <div class="col-md-12 mt-2">
            <label>Валюта:&nbsp;</label>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="radio_currency" id="radio_currency_rur" value="rur">
                <label class="form-check-label" for="inlineRadio1">Рубли</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="radio_currency" id="radio_currency_usd" value="usd">
                <label class="form-check-label" for="inlineRadio2">Доллары</label>
            </div>
            <div style="float: right;">
                <a href="test.docx" >Описание задания</a>
            </div>
            <p class="text-secondary font-weight-light font-italic">
                Не самый заметный момент в задании: зарплата сотрудника хранится
                в таблице workers и дублируется в таблице payment, но с учетом премии.
            </p>
        </div>

    </div>
</div>
<!-- // top menu -->

<!-- add worker modal -->
<div id="c_add_worker" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Добавить сотрудника</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

<form>
    <div class="form-group">
        <label for="first_name">Имя</label>
        <input type="text" class="form-control" id="first_name" placeholder="">
    </div>
    <div class="form-group">
        <label for="last_name">Фамилия</label>
        <input type="text" class="form-control" id="last_name" placeholder="">
    </div>
    <div class="form-group">
        <label for="profession">Должность</label>
        <select class="form-control" id="profession">
<?php

$workers->printProfessions();

?>
        </select>
    </div>
    <div class="form-group">
        <label for="salary">Заработная плата</label>
        <input type="text" class="form-control" id="salary" placeholder="">
    </div>
</form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button id="btn_add_worker" type="button" class="btn btn-primary">Сохранить</button>
            </div>
        </div>
    </div>
</div>
<!-- // add worker modal -->

<!-- upload_photo modal -->
<div id="c_upload_photo" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Загрузить фото</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

<form>
    <div class="form-group">
        <label for="photo_file">Выбирете фото</label>
        <input type="file" class="form-control-file" id="photo_file">
    </div>
</form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button id="btn_photo_upload" type="button" class="btn btn-primary">Загрузить</button>
            </div>
        </div>
    </div>
</div>
<!-- // upload_photo modal -->

<!-- Таблица сотрудников -->
<div id="c_workers" class="container-fluid" style="margin-top: .2rem;">
    <div class="row">
        <div class="col-12">

<table id='t_workers' class="table table-striped">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Месяц</th>
            <th scope="col">Имя</th>
            <th scope="col">Фамилия</th>
            <th scope="col">Должность</th>
            <th scope="col">Зарплата</th>
            <th scope="col">Выплата</th>
            <th scope="col">Фото</th>
        </tr>
    </thead>
    <tbody>

<?php

$workers->printPaymentsByPeriod();

?>

    </tbody>
</table>

        </div>
    </div>
</div>
<!-- // Таблица сотрудников -->

</body>

</html>
