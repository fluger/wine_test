$( document ).ready(function() {

    $('#datepicker').datepicker({language: 'ru', format: 'yyyy-mm-05'}); // выбор даты
    $('#datepicker').val(''); // после перезагрузки страницы браузер сохраняет значение поля, очищаем его

    $('.image-link').viewbox(); // плагин для просмотра изображений

    var reloadWorkers = function(period) {
        if ( typeof period === 'undefined' ) period = $('#datepicker').val();
        $.ajax({
            type: "POST",
            url: "getWorkers.php",
            data: { period: period }
        }).done(function(result) {
            $('#t_workers').find('tbody').html(result);
            $('.image-link').viewbox(); // обновляем viewbox
        }).fail(function(){
            alert('ошибка загрузки данных');
        });
    };

    // обновляем данные после изменения периода
    // проверяем что период был действительно изменен что бы не загружать одни и те же данные повторно
    var prev_date_val = '';
    $('#datepicker').on('change', function() {
        var date_val = $('#datepicker').val();
        if ( date_val !== prev_date_val ) {
            prev_date_val = date_val;
            reloadWorkers(date_val);
        }
    });

    // добавление сотрудника
    var add_worker_form = $('#c_add_worker');
    $('#btn_add_worker').click(function(e) {
        var first_name = add_worker_form.find('#first_name').val();
        if ( !first_name ) {
            add_worker_form.find('#first_name').addClass('is-invalid');
            return;
        }
        var last_name = add_worker_form.find('#last_name').val();
        if ( !last_name ) {
            add_worker_form.find('#last_name').addClass('is-invalid');
            return;
        }
        var salary = add_worker_form.find('#salary').val();
        if ( !salary ) {
            add_worker_form.find('#salary').addClass('is-invalid');
            return;
        }
        var profession = parseInt(add_worker_form.find('#profession').val());
        $.ajax({
            type: "POST",
            url: "addWorker.php",
            data: {
                first_name: first_name,
                last_name: last_name,
                salary: salary,
                profession_id: profession
            }
        }).done(function(result) {
            if ( result.success ) {
                alert('сотрудник добавлен');
                reloadWorkers();
            } else {
                alert(result.error);
            }
        }).fail(function(){
            alert('ошибка');
        });
    });
    // убираем отметку invalid по событию on change
    var setValid = function() {
        $(this).removeClass('is-invalid');
    };
    add_worker_form.find('#first_name').on('change', setValid);
    add_worker_form.find('#last_name').on('change', setValid);
    add_worker_form.find('#salary').on('change', setValid);

    // курс
    var getCourse = function() {
        $('#course').val('');
        $.ajax({
            type: "POST",
            url: "getCourse.php"
        }).done(function(result) {
            if ( result.success )
                $('#course').val( result.data );
        }).fail(function(){
            alert('ошибка загрузки данных');
        });
    };
    getCourse(); // подгружаем курс аяксом после загрузки документа

    // установка курса
    $('#btn_set_course').click(function() {
        var course = $('#course').val();
        $.ajax({
            type: "POST",
            // contentType: "application/json",
            url: "setCourse.php",
            data: { course: course }
        }).done(function(result) {
            if ( result.success ) {
                alert('курс установлен');
            }
            getCourse();
            reloadWorkers();
        }).fail(function(){
            alert('ошибка');
        });
    });

    // функции для работы с куками (взято из интернета)
    var getCookie = function(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    }
    var setCookie = function(name, value, props) {
        props = props || {}
        var exp = props.expires
        if (typeof exp == "number" && exp) {
            var d = new Date()
            d.setTime(d.getTime() + exp*1000)
            exp = props.expires = d
        }
        if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }
        value = encodeURIComponent(value)
        var updatedCookie = name + "=" + value
        for(var propName in props) {
            updatedCookie += "; " + propName
            var propValue = props[propName]
            if(propValue !== true){ updatedCookie += "=" + propValue }
        }
        document.cookie = updatedCookie;
    }

    // получаем валюту пользователя из куки
    var user_currency = getCookie('currency');
    if ( !user_currency ) {
        user_currency = 'rur';
        // устанавливаем куку на 7 дней
        setCookie('currency', user_currency, {expires: (60*24*7)});
    }
    // меняем переключатель в зависимости от установленой куки
    $('#radio_currency_rur').prop('checked', (user_currency === 'rur') );
    $('#radio_currency_usd').prop('checked', (user_currency === 'usd') );
    // отслеживаем событие change на переключателе валюты. обновляем куку
    $('input[type=radio][name=radio_currency]').change(function() {
        var new_currency = $('input[type=radio][name=radio_currency]:checked').val();
        setCookie('currency', new_currency, {expires: (60*24*7)});
        user_currency = new_currency;
        reloadWorkers();
    });

    // выдача премии
    $('#btn_add_premium').click(function() {
        var amount = parseFloat($('#premium_amount').val());
        if ( !amount ) {
            alert('Неправильная сумма');
            return;
        };
        var profession_id = parseInt($('#premium_profession').val());
        var period = $('#datepicker').val();
        $.ajax({
            type: "POST",
            url: "addPremium.php",
            data: {
                amount: amount,
                profession_id: profession_id,
                period: period
            }
        }).done(function(result) {
            if ( result.success ) {
                alert('премия добавлена');
                reloadWorkers();
            } else {
                alert(result.error);
            }
        }).fail(function(){
            alert('ошибка');
        });
    });

    // загрузка фото
    var photo_upload_for = 0;
    uploadPhoto = function(e) {
        e.preventDefault();
        photo_upload_for = $(e.target).parents('tr').find('th').text()
        $('#c_upload_photo').modal();
    }

    $('#btn_photo_upload').click(function() {
        var file_data = $('#photo_file').prop('files')[0];
        var form_data = new FormData();
        form_data.append('photo', file_data);
        form_data.append('worker_id', photo_upload_for);

        $.ajax({
            url: 'uploadPhoto.php',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post'
        }).done(function(result) {
            if ( result.success ) {
                alert('фото обновлено');
                $('#c_upload_photo').modal("hide")
                reloadWorkers();
            } else {
                alert(result.error);
            }
        }).fail(function(){
            alert('ошибка');
        });
    });

});
