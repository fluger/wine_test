CREATE DATABASE  IF NOT EXISTS `test_w` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `test_w`;
-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: z3055.ru    Database: test_w
-- ------------------------------------------------------
-- Server version	5.7.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `course` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (65);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` date DEFAULT NULL,
  `worker_id` int(11) DEFAULT NULL,
  `salary` double DEFAULT NULL COMMENT 'worker salary for the current month',
  `amount` double DEFAULT NULL COMMENT 'paid amount including premium',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (127,'2019-07-05',1,76197.46548700842,83415.15361649464),(128,'2019-07-05',2,93303.64441806791,97527.84866823615),(129,'2019-07-05',3,87925.8271659965,96993.78433571549),(130,'2019-07-05',4,59718.204112461026,62185.37751788774),(131,'2019-07-05',5,84813.54060099789,90245.53642631092),(132,'2019-07-05',6,94913.09220428867,104671.5514219101),(133,'2019-07-05',7,70124.84075513188,72621.14967463646),(134,'2019-07-05',8,65884.92869947565,69091.09595147065),(135,'2019-07-05',9,69050.12276866485,77692.03257746252),(136,'2019-07-05',10,97595.82928157954,100786.87687691941),(137,'2019-07-05',11,80828.77963858543,90658.28849622821),(138,'2019-07-05',12,61356.411884870766,71230.81369440147),(139,'2019-07-05',13,64295.72204527978,74179.20482734144),(140,'2019-07-05',14,87409.37610846886,97303.58489752149),(141,'2019-07-05',15,94159.71594318721,103480.31184960186),(142,'2019-08-05',1,76197.46548700842,83417.8189592602),(143,'2019-08-05',2,93303.64441806791,101443.6243674193),(144,'2019-08-05',3,87925.8271659965,96964.66680333453),(145,'2019-08-05',4,59718.204112461026,60492.4627584335),(146,'2019-08-05',5,84813.54060099789,91568.31522618263),(147,'2019-08-05',6,94913.09220428867,96364.18969963136),(148,'2019-08-05',7,70124.84075513188,77116.0046636276),(149,'2019-08-05',8,65884.92869947565,66487.45606326262),(150,'2019-08-05',9,69050.12276866485,71089.26816944896),(151,'2019-08-05',10,97595.82928157954,105983.97450147566),(152,'2019-08-05',11,80828.77963858543,86652.06984305017),(153,'2019-08-05',12,61356.411884870766,65308.427554842485),(154,'2019-08-05',13,64295.72204527978,66585.93008908063),(155,'2019-08-05',14,87409.37610846886,97004.36962489446),(156,'2019-08-05',15,94159.71594318721,95264.05970124906),(157,'2019-09-05',1,76197.46548700842,83934.20403537732),(158,'2019-09-05',2,93303.64441806791,158674.30619306333),(159,'2019-09-05',3,87925.8271659965,154568.92070320336),(160,'2019-09-05',4,59718.204112461026,66821.68678084546),(161,'2019-09-05',5,84813.54060099789,85401.67363863594),(162,'2019-09-05',6,94913.09220428867,96543.30969466205),(163,'2019-09-05',7,70124.84075513188,77511.52940142111),(164,'2019-09-05',8,65884.92869947565,72927.71976713811),(165,'2019-09-05',9,69050.12276866485,140104.01247544587),(166,'2019-09-05',10,97595.82928157954,171736.9049198338),(167,'2019-09-05',11,80828.77963858543,88372.48901685),(168,'2019-09-05',12,61356.411884870766,62651.732168767354),(169,'2019-09-05',13,64295.72204527978,73141.19563730544),(170,'2019-09-05',14,87409.37610846886,155750.7835242439),(171,'2019-09-05',15,94159.71594318721,95330.3325533218),(172,'2019-10-05',1,76197.46548700842,82636.32659769275),(173,'2019-10-05',2,93303.64441806791,97936.10044842224),(174,'2019-10-05',3,87925.8271659965,94601.5242930516),(175,'2019-10-05',4,59718.204112461026,67199.32196400997),(176,'2019-10-05',5,84813.54060099789,85192.03878491375),(177,'2019-10-05',6,94913.09220428867,104362.22987655758),(178,'2019-10-05',7,70124.84075513188,81845.03487206533),(179,'2019-10-05',8,65884.92869947565,68088.48657467263),(180,'2019-10-05',9,69050.12276866485,72737.33010118578),(181,'2019-10-05',10,97595.82928157954,105421.19262592978),(182,'2019-10-05',11,80828.77963858543,86893.97467011024),(183,'2019-10-05',12,61356.411884870766,76816.29731678059),(184,'2019-10-05',13,64295.72204527978,65349.56441759091),(185,'2019-10-05',14,87409.37610846886,94128.9319816315),(186,'2019-10-05',15,94159.71594318721,105205.96849940342),(190,'2019-07-05',17,6716.54,6726.54),(191,'2019-07-05',19,35000,35010),(193,'2019-10-05',17,6716.54,7716.54),(194,'2019-10-05',19,35000,36000),(196,'2019-09-05',17,6716.54,71716.54),(197,'2019-09-05',19,35000,100000);
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professions`
--

DROP TABLE IF EXISTS `professions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professions`
--

LOCK TABLES `professions` WRITE;
/*!40000 ALTER TABLE `professions` DISABLE KEYS */;
INSERT INTO `professions` VALUES (1,'бухгалтер'),(2,'курьер'),(3,'менеджер');
/*!40000 ALTER TABLE `professions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workers`
--

DROP TABLE IF EXISTS `workers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `profession_id` int(11) DEFAULT NULL,
  `salary` double DEFAULT NULL,
  `photo` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workers`
--

LOCK TABLES `workers` WRITE;
/*!40000 ALTER TABLE `workers` DISABLE KEYS */;
INSERT INTO `workers` VALUES (1,'Самсон','Кузарин',1,76197.46548700842,'photos/no_photo.png'),(2,'Натан','Болотников',2,93303.64441806791,'photos/man_p_1.jpg'),(3,'Ульяна','Яркова',2,87925.8271659965,'photos/beautiful-blur-blurred-background-733872.jpg'),(4,'Инна','Якимович',3,59718.204112461026,'photos/pexels-photo-1036623.jpeg'),(5,'Артем','Куцак',3,84813.54060099789,'photos/man_2.jpg'),(6,'Касьян','Синай',3,94913.09220428867,'photos/pexels-photo-1222271.jpeg'),(7,'Алиса','Костомарова',1,70124.84075513188,'photos/no_photo.png'),(8,'Сигизмунд','Кобзев',3,65884.92869947565,'photos/no_photo.png'),(9,'Роман','Мозговой',2,69050.12276866485,'photos/no_photo.png'),(10,'Остап','Ивкин',2,97595.82928157954,'photos/no_photo.png'),(11,'Евдокия','Клемент',3,80828.77963858543,'photos/no_photo.png'),(12,'Наталия','Швардыгула',1,61356.411884870766,'photos/no_photo.png'),(13,'Егор','Андрюхин',3,64295.72204527978,'photos/no_photo.png'),(14,'Захар','Явленский',2,87409.37610846886,'photos/no_photo.png'),(15,'Емельян','Брязгин',1,94159.71594318721,'photos/no_photo.png'),(17,'Иван','Маршаков',2,6716.54,'photos/no_photo.png'),(18,'Семён','Гла\'\"<?php echo \'123\' ;?>*згов',1,67165.4,'photos/no_photo.png'),(19,'Иван','Иванов',2,35000,'photos/no_photo.png');
/*!40000 ALTER TABLE `workers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-14  9:15:16
