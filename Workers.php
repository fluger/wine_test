<?php

require_once __DIR__ . '/config.php';

Class Workers {

    private $dbh;

    private $user_currency;
    private $course_modifier;

    public function __construct() {
        // получаем валюту из куки. все денежные опирации проводим согласно установленной валюте
        // в базе все данные храним в рублях
        @$this->user_currency = $_COOKIE['currency'];
        if ( !$this->user_currency ) $this->user_currency = 'rur';
        $this->course_modifier = $this->user_currency === 'usd'
            ? $this->getCourse()
            : 1
        ;
    }

    protected function getDb() {
        if ( $this->dbh ) return $this->dbh;

        $dsn = sprintf('mysql:host=%s;dbname=%s;charset=%s', DB_HOST, DB_DATABASE, DB_CHARSET);

        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $this->dbh = new PDO($dsn, DB_USER, DB_PASS, $opt);
        return $this->dbh;
    }

    // не самая лучшая реализация, подобные конструкции обычно сложно поддерживать ввиду того что
    // можно легко ошибится в количестве или последовательности аргументов sprintf
    // проще было бы подключить какой-нибудь шаблонизатор
    public function getPaymentHtml($payment_sql) {
        $currency = strtoupper($this->user_currency);
        $tmpl = <<<EOF
<tr>
    <th scope="row">%d</th>
    <td>%s</td>
    <td>%s</td>
    <td>%s</td>
    <td>%s</td>
    <td>%.2f $currency</td>
    <td>%.2f $currency<br/><p class="text-success">(+%.2f $currency)</p></td>
    <td>
        <a href="%s" class="image-link">
            <img src="%s" class="img-thumbnail">
        </a>
        <br/>
        <a href='#' onclick="uploadPhoto(event);">загрузить фото</a>
    </td>
</tr>
EOF;
        return sprintf(
            $tmpl,
            $payment_sql['worker_id'],
            $payment_sql['period'],
            $payment_sql['first_name'],
            $payment_sql['last_name'],
            $payment_sql['profession_name'],
            $payment_sql['salary'] / $this->course_modifier,
            $payment_sql['amount'] / $this->course_modifier,
            $payment_sql['premium'] / $this->course_modifier,
            $payment_sql['photo'],
            str_replace('photos/','photos/thumb_',$payment_sql['photo'])
        );
    }

    // проверяет переданую дату, если дата не правильная то установить текущий месяц
    protected function getValidPeriod($period) {
        $is_valid_date = preg_match('/^[\d]{4}-[\d]{2}-05$/', $period, $m);
        if (!$period || $is_valid_date !== 1) $period = sprintf("%04d-%02d-05", date("Y"), date("m"));
        return $period;
    }

    // выводит данные во всем сотрудникам в html формате
    // добавляет данные по выплатам из таблицы payments за указанный период (если имеется)
    public function printPaymentsByPeriod($period='') {
        $dbh = $this->getDb();

        $period = $this->getValidPeriod($period);

        // "выплата" отображается только при наличии записей в таблице payments
        $query = <<<EOF
SELECT
    w.id as worker_id,
    -- p.period as period,
    COALESCE(
        p.period,
        CAST(DATE_FORMAT(NOW(),'%Y-%m-05') as DATE)
    ) as period,
    w.first_name,
    w.last_name,
    prof.name as profession_name,
    COALESCE(p.salary, w.salary) as salary,
    p.amount,
    -- ниже код который позволит отображать выплату не зависимо от наличия
    -- записей в таблице payments
    -- COALESCE(p.amount, w.salary) as amount,
    (p.amount - p.salary) as premium,
    w.photo
FROM workers w
LEFT JOIN payments p ON (p.worker_id = w.id and p.period = :period)
LEFT JOIN professions prof ON (w.profession_id = prof.id) 
ORDER by w.id;
EOF;

        $sth = $dbh->prepare($query);
        $sth->execute(array(':period' => $period));

        while ($row = $sth->fetch()) {
            echo $this->getPaymentHtml($row);
        }
    }

    public function getCourse() {
        $dbh = $this->getDb();

        $query = 'SELECT course FROM course LIMIT 1';
        $sth = $dbh->prepare($query);
        $sth->execute();
        $result = $sth->fetch();

        if ( !$result ) {
            throw new Exception('Error getting course');
        }

        $course = round(floatval($result['course']),4);
        return $course;
    }

    public function printCourse() {
        $course = $this->getCourse();
        echo json_encode(['success'=>true, 'data'=>$course]);
    }

    public function setCourse($course) {
        $course = floatval($course);
        if ( $course <= 0 ) {
            echo json_encode(['success'=>false]);
            return;
        }

        $dbh = $this->getDb();
        $query = 'UPDATE course SET course = :course';
        $sth = $dbh->prepare($query);
        $sth->execute([$course]);

        echo json_encode(['success'=>true, 'data'=>$course]);
    }

    public function printProfessions() {
        $dbh = $this->getDb();
        $query = 'SELECT id, name FROM professions';
        $sth = $dbh->prepare($query);
        $sth->execute();

        while ($row = $sth->fetch()) {
            echo sprintf('<option value="%d">%s</option>', $row['id'], $row['name']);
        }
    }

    protected function checkProfessionId($profession_id) {
        $dbh = $this->getDb();
        $query = 'SELECT 1 FROM professions WHERE id = :profession_id';
        $sth = $dbh->prepare($query);
        $sth->execute([$profession_id]);

        $res = $sth->fetch();
        // вернет 0 строк в случае если указан не правильный profession_id
        return (bool)$res;
    }

    public function addWorker($first_name, $last_name, $salary, $profession_id) {
        $profession_id = intVal($profession_id);
        if ( !$this->checkProfessionId($profession_id) ) {
            echo json_encode(['success'=>false, 'error'=>'Invalid request']);
            return;
        }
        if ( !$first_name || !$last_name ) {
            echo json_encode(['success'=>false, 'error'=>'Invalid request']);
            return;
        }
        $salary = floatval($salary);
        if ( !$salary || $salary <= 0 ) {
            echo json_encode(['success'=>false, 'error'=>'Invalid request']);
            return;
        }

        $dbh = $this->getDb();
        $query  = 'INSERT INTO workers(first_name, last_name, profession_id, salary, photo) ';
        $query .= 'VALUES(:first_name,:last_name,:profession_id,:salary,\'photos/no_photo.png\')';
        $sth = $dbh->prepare($query);
        $data = array(
            ':first_name'    => $first_name,
            ':last_name'     => $last_name,
            ':profession_id' => $profession_id,
            ':salary'        => $salary * $this->course_modifier
        );
        $sth->execute($data);

        echo json_encode(['success'=>true]);
    }

    public function addPremium($amount, $profession_id, $period) {
        $profession_id = intVal($profession_id);
        if ( !$this->checkProfessionId($profession_id) ) {
            echo json_encode(['success'=>false, 'error'=>'Invalid request']);
            return;
        }
        $amount = floatval($amount);
        if ( !$amount || $amount <= 0 ) {
            echo json_encode(['success'=>false, 'error'=>'Invalid request']);
            return;
        }
        $premium = $amount * $this->course_modifier;
        $period = $this->getValidPeriod($period);

        $dbh = $this->getDb();
        $dbh->beginTransaction(); // выдача премии выполняется в рамках транзакции
                                  // если второй запрос провалится, первый будет отменен

        try {
            // добавляем премию для сотрудников уже имеющих выплаты за заданный период
            $query = <<<EOF
UPDATE payments p
LEFT JOIN workers w ON (p.worker_id = w.id)
SET p.amount = (p.amount + :amount)
WHERE
    p.period = :period AND
    w.profession_id = :profession_id
EOF;
            $sth = $dbh->prepare($query);
            $data = array(
                ':period'        => $period,
                ':profession_id' => $profession_id,
                ':amount'        => ($amount * $this->course_modifier)
            );
            $sth->execute($data);
            // добавляем записи в таблицу payments для сотрудников еще не имеющих выплат
            $query = <<<EOF
INSERT INTO payments(period,worker_id,salary,amount)
SELECT
    :period1,
    w.id,
    w.salary,
    (w.salary + :amount)
FROM workers w
WHERE
    w.profession_id = :profession_id AND
    not exists(SELECT 1 FROM payments p WHERE p.worker_id = w.id and p.period = :period2)
EOF;
            $sth = $dbh->prepare($query);
            $data = array(
                ':period1'       => $period,
                ':period2'       => $period,
                ':profession_id' => $profession_id,
                ':amount'        => ($amount * $this->course_modifier)
            );
            $sth->execute($data);
        } catch (Exception $e) {
            $dbh->rollBack();
            throw $e;
        }
        $dbh->commit();

        echo json_encode(['success'=>true]);
    }

    public function uploadPhoto($worker_id) {
        // перемещаем файл в каталог /photos/
        if ( !$_FILES['photo'] ) {
            throw new Exception('Internal error');
        }
        $imagetype = $_FILES['photo']['type'];
        if ( $imagetype !== 'image/png' && $imagetype !== 'image/jpeg' ) {
            echo json_encode(['success'=>false, 'error'=>'Only png and jpeg images are supported']);
            return;
        }
        $tmp_name = $_FILES['photo']['tmp_name'];
        $file_name = $_FILES['photo']['name'];
        $file_name = preg_replace('/[^A-Za-z0-9_.\-]/', '_', $file_name);
        $file_path = __DIR__ . "/photos/" . $file_name;
        if ( file_exists($file_path) ) {
            echo json_encode(['success'=>false, 'error'=>'File exists']);
            return;
        }
        if ( !rename($tmp_name, $file_path) ) {
            throw new Exception('Internal error');
        }

        // создаем миниатюру
        $is_jpeg = ($imagetype === 'image/jpeg');
        $image = $is_jpeg ? imagecreatefromjpeg($file_path) : imagecreatefrompng($file_path);
        if ( !$image ) {
            unlink($file_path);
            throw new Exception('Internal error');
        }
        $width  = imagesx($image);
        $height = imagesy($image);
        $targetHeight = null;
        $targetWidth = 150;
        $ratio = $width / $height;
        if ($width > $height) {
            $targetHeight = floor($targetWidth / $ratio);
        } else {
            $targetHeight = $targetWidth;
            $targetWidth = floor($targetWidth * $ratio);
        }
        $thumbnail = imagecreatetruecolor($targetWidth, $targetHeight);
        if ( !$is_jpeg ) {
            imagecolortransparent($thumbnail,imagecolorallocate($thumbnail, 0, 0, 0));
            imagealphablending($thumbnail, false);
            imagesavealpha($thumbnail, true);
        }
        imagecopyresampled(
            $thumbnail,
            $image,
            0, 0, 0, 0,
            $targetWidth, $targetHeight,
            $width, $height
        );
        if ( $is_jpeg ) {
            imagejpeg($thumbnail, __DIR__ . "/photos/thumb_".$file_name );
        } else {
            imagepng($thumbnail, __DIR__ . "/photos/thumb_".$file_name );
        }

        // обновляем запись в базе
        $dbh = $this->getDb();
        $query = 'UPDATE workers SET photo = :path WHERE id = :worker_id';
        $sth = $dbh->prepare($query);
        $sth->execute(array(
            ':path' => 'photos/'.$file_name,
            ':worker_id' => intVal($worker_id)
        ));

        echo json_encode(['success'=>true]);
    }

}

?>
