CREATE DATABASE test_w character set UTF8;
GRANT ALL PRIVILEGES ON test_w.* TO 'test_w_user'@'localhost' IDENTIFIED BY 'secret21';
FLUSH PRIVILEGES;

USE test_w;

/* ################################################# */

CREATE TABLE professions(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(32)
);

-- START TRANSACTION;
-- COMMIT;

INSERT INTO professions(name) VALUES
('бухгалтер'),
('курьер'),
('менеджер')
;

/* ################################################# */

CREATE TABLE workers(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(32),
    last_name VARCHAR(32),
    profession_id INT,
    salary DOUBLE,
    photo TEXT
);

-- (RAND()*50000+50000) случайное число от >= 50000 до < 100000 (например 59718.204112461026)
INSERT INTO workers VALUES
(1,'Самсон','Кузарин',1,(RAND()*50000+50000),'photos/no_photo.png'),
(2,'Натан','Болотников',2,(RAND()*50000+50000),'photos/no_photo.png'),
(3,'Ульяна','Яркова',2,(RAND()*50000+50000),'photos/no_photo.png'),
(4,'Инна','Якимович',3,(RAND()*50000+50000),'photos/no_photo.png'),
(5,'Артем','Куцак',3,(RAND()*50000+50000),'photos/man_2.jpg'),
(6,'Касьян','Синай',3,(RAND()*50000+50000),'photos/no_photo.png'),
(7,'Алиса','Костомарова',1,(RAND()*50000+50000),'photos/no_photo.png'),
(8,'Сигизмунд','Кобзев',3,(RAND()*50000+50000),'photos/no_photo.png'),
(9,'Роман','Мозговой',2,(RAND()*50000+50000),'photos/no_photo.png'),
(10,'Остап','Ивкин',2,(RAND()*50000+50000),'photos/no_photo.png'),
(11,'Евдокия','Клемент',3,(RAND()*50000+50000),'photos/no_photo.png'),
(12,'Наталия','Швардыгула',1,(RAND()*50000+50000),'photos/no_photo.png'),
(13,'Егор','Андрюхин',3,(RAND()*50000+50000),'photos/no_photo.png'),
(14,'Захар','Явленский',2,(RAND()*50000+50000),'photos/no_photo.png'),
(15,'Емельян','Брязгин',1,(RAND()*50000+50000),'photos/no_photo.png')
;

-- UPDATE WORKERS SET photo='photos/man_2.jpg' WHERE id = 5;

/* ################################################# */

CREATE TABLE payments(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    period DATE,
    worker_id INT,
    salary DOUBLE COMMENT 'worker salary for the current month',
    amount DOUBLE COMMENT 'paid amount including premium'
);

-- заполняем таблицу payments
-- делаем выборку по всем сотрудникам. получаем зарплату и зарплату со случайно сгенерированной надбавкой (от 0 до 10000)
-- на нее джойним таблицу t1 содержащую четыре прошедших месяца(включая текущий)
-- результат добавляем в таблицу payments
-- ( вообще подобных сложных конструкций стараюсь по возможности избегать )
INSERT INTO payments(period, worker_id, salary, amount)
(
    SELECT
        t1.period as period,
        w.id as worder_id,
        w.salary as salary,
        w.salary + (RAND()*10000) as amount
    FROM workers w
    LEFT JOIN (
            SELECT CAST(DATE_FORMAT(NOW() - INTERVAL 3 MONTH,'%Y-%m-05') as DATE) as period
            UNION ALL
            SELECT CAST(DATE_FORMAT(NOW() - INTERVAL 2 MONTH,'%Y-%m-05') as DATE) as period
            UNION ALL
            SELECT CAST(DATE_FORMAT(NOW() - INTERVAL 1 MONTH,'%Y-%m-05') as DATE) as period
            UNION ALL
            SELECT CAST(DATE_FORMAT(NOW(),'%Y-%m-05') as DATE) as period
    ) as t1 ON (1=1)
)
;

/* ################################################# */

-- таблица содержит только одну строку, последний установленный курс
CREATE TABLE course(
    course DOUBLE
);

INSERT INTO course VALUES(64.1234);

/* ################################################# */

-- Кузарин Самсон
-- Болотников Натан
-- Яркова Ульяна
-- Якимович Инна
-- Куцак Артем
-- Синай Касьян
-- Костомарова Алиса
-- Кобзев Сигизмунд
-- Мозговой Роман
-- Ивкин Остап
-- Клемент Евдокия
-- Швардыгула Наталия
-- Андрюхин Егор
-- Явленский Захар
-- Брязгин Емельян



;

