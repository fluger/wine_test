<?php

require_once __DIR__ . '/Workers.php';

@$first_name = $_POST['first_name'];
@$last_name = $_POST['last_name'];
@$salary = $_POST['salary'];
@$profession_id = $_POST['profession_id'];

header('Content-Type: application/json');

$workers = new Workers();
$workers->addWorker($first_name, $last_name, $salary, $profession_id);

?>
